import React, { Component } from 'react';
import PropTypes from 'prop-types';
import gql from 'graphql-tag';
import { withApollo, graphql } from 'react-apollo';
import MessageList from './MessageList';

import UserInput from './UserInput';
import Header from './Header';

const MESSSAGES_QUERY_CLIENT = gql`
  query messages {
    messages @client {
      _id
      senderId
      recepientId
      conversationId
      platformName
      organizationId
      createdAt
      payload {
        ... on Text {
          type
          text
        }
        ... on Media {
          type
          url
          caption
          name
        }
      }
    }
  }
`;

const WEBCHAT_NEW_MESSAGE_SUBSCRIPTION = gql`
  subscription webchatNewMessage($conversationId: String!) {
    webchatNewMessage(conversationId: $conversationId) {
      _id
      senderId
      recepientId
      conversationId
      platformName
      organizationId
      createdAt
      payload {
        ... on Text {
          type
          text
        }
        ... on Media {
          type
          url
          caption
          name
        }
      }
    }
  }
`;

class ChatBox extends Component {
  state = {
    me: JSON.parse(sessionStorage.getItem('webchat_me')),
  };

  onUserInputSubmit = message => {
    this.props.onUserInputSubmit(message);
  };

  componentWillUnmount() {
    this.state.unsubscribe();
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (prevState.unsubscribe) {
      return prevState;
    }
    return {
      ...prevState,
      unsubscribe:
        nextProps.data &&
        nextProps.data.subscribeToMore({
          document: WEBCHAT_NEW_MESSAGE_SUBSCRIPTION,
          variables: {
            conversationId: prevState.me.conversationId,
          },
          updateQuery: (prev, { subscriptionData }) => {
            if (!subscriptionData.data) {
              return prev;
            }

            const messageSave = subscriptionData.data.webchatNewMessage;
            // const { conversationId, recepientId } = messageSave;
            console.log({ messageSave });
            // don't double add the message
            // if (!prev.data.messages.find(msg => msg._id === messageSave._id)) {
            const { messages } = nextProps.client.readQuery({
              query: MESSSAGES_QUERY_CLIENT,
            });
            // console.log({ messages });
            if (Array.isArray(messages)) {
              const newMessages = [...messages, messageSave];
              nextProps.client.writeQuery({
                query: MESSSAGES_QUERY_CLIENT,
                data: {
                  messages: newMessages,
                },
              });
              return {
                messages: newMessages,
              };
            }
            // },
          },
        }),
    };
  }

  render() {
    const {
      data: { messages = [], fetchMore, variables, error: err, refetch },
    } = this.props;
    console.log(messages);
    return (
      <React.Fragment>
        <Header
          teamName={this.props.agentProfile.teamName}
          imageUrl={this.props.agentProfile.imageUrl}
          onClose={this.props.onClose}
        />
        <MessageList
          messages={messages}
          imageUrl={this.props.agentProfile.imageUrl}
          onDelete={this.props.onDelete}
        />
        <UserInput
          showEmoji={false}
          onSubmit={this.onUserInputSubmit}
          showFile={false}
          onKeyPress={this.props.onKeyPress}
        />
      </React.Fragment>
    );
  }
}

export default graphql(MESSSAGES_QUERY_CLIENT)(withApollo(ChatBox));
