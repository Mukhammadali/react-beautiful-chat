import PropTypes from 'prop-types';
import React, { Component } from 'react';

import ChatWindow from './ChatWindow';

class Launcher extends Component {
  constructor() {
    super();
    this.state = {
      launcherIcon:
        'https://drbot.sfo2.digitaloceanspaces.com/media/logo-no-bg.svg',
      isOpen: false,
    };
  }

  handleClick = () => {
    if (this.props.handleClick !== undefined) {
      this.props.handleClick();
    } else {
      this.setState({
        isOpen: !this.state.isOpen,
      });
    }
  };

  render() {
    const isOpen = this.props.hasOwnProperty('isOpen')
      ? this.props.isOpen
      : this.state.isOpen;
    const classList = ['sc-launcher', isOpen ? 'opened' : ''];

    return (
      <React.Fragment>
        <div>
          <div className={classList.join(' ')} onClick={this.handleClick}>
            <MessageCount count={this.props.newMessagesCount} isOpen={isOpen} />
            <img
              className="sc-open-icon"
              src="https://drbot.sfo2.digitaloceanspaces.com/media/close-icon.png"
            />
            <img
              className="sc-closed-icon"
              src="https://drbot.sfo2.digitaloceanspaces.com/media/chat-icon.svg"
            />
          </div>
          <ChatWindow
            onUserInputSubmit={this.props.onMessageWasSent}
            agentProfile={this.props.agentProfile}
            isOpen={isOpen}
            onClose={this.handleClick}
            showEmoji={this.props.showEmoji}
            showFile={this.props.showFile}
            onKeyPress={this.props.onKeyPress}
            onKeyPressDebounce={this.props.onKeyPressDebounce}
            onDelete={this.props.onDelete}
          />
        </div>
      </React.Fragment>
    );
  }
}

const MessageCount = props => {
  if (props.count === 0 || props.isOpen === true) {
    return null;
  }
  return <div className="sc-new-messsages-count">{props.count}</div>;
};

Launcher.propTypes = {
  onMessageWasReceived: PropTypes.func,
  onMessageWasSent: PropTypes.func,
  newMessagesCount: PropTypes.number,
  isOpen: PropTypes.bool,
  handleClick: PropTypes.func,
  messageList: PropTypes.arrayOf(PropTypes.object),
  showEmoji: PropTypes.bool,
  showFile: PropTypes.bool,
  onKeyPress: PropTypes.func,
  onDelete: PropTypes.func,
};

Launcher.defaultProps = {
  newMessagesCount: 0,
};

export default Launcher;
