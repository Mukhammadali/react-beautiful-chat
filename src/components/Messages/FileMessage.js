import React from 'react';

const FileMessage = props => {
  const { url, name, caption } = props.message.payload;
  return (
    <div className="sc-message--file">
      <div className="sc-message--file-icon">
        <a href={url || '#'} target="_blank">
          <img
            src="https://drbot.sfo2.digitaloceanspaces.com/media/file.svg"
            alt="generic file icon"
            height={60}
          />
        </a>
      </div>
      <div className="sc-message--file-name">
        <a href={url} target="_blank">
          Open File
        </a>
      </div>
      {/* <div className="sc-message--file-name">
        <a href={url} target="_blank">
          {name}
        </a>
      </div>
      <div className="sc-message--file-text">{caption || ''}</div> */}
    </div>
  );
};

export default FileMessage;
