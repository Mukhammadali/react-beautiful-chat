/* eslint-disable jsx-a11y/media-has-caption */
import React from 'react';

const VideoMessage = props => {
  const { url, name, caption } = props.message.payload;
  return (
    <div className="sc-message--file">
      <div>
        {/* <a href={url || '#'} target="_blank"> */}
        <video className="responsive" autoPlay controls>
          <source src={url} type="video/mp4" />
          Your browser does not support the video tag.
        </video>
        {/* </a> */}
      </div>
      <div className="sc-message--file-text">{caption || ''}</div>
    </div>
  );
};

export default VideoMessage;
