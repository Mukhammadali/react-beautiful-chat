import React from 'react';

const ImageMessage = props => {
  const { url, name, caption } = props.message.payload;
  return (
    <div className="sc-message--file">
      <div className="">
        <a href={url || '#'} target="_blank">
          <img
            className="responsive"
            src={url}
            alt="attachment_image"
            height={60}
          />
        </a>
      </div>
      <div className="sc-message--file-text">{caption || ''}</div>
    </div>
  );
};

export default ImageMessage;
