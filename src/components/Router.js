import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ChatBox from './ChatBox';
import Auth from '../containers/Auth';

export default class Router extends Component {
  state = {
    loggedIn: JSON.parse(sessionStorage.getItem('webchat_logged_in')) || false,
  };

  toggleLoggedIn = () => {
    this.setState({ loggedIn: true });
    sessionStorage.setItem('webchat_logged_in', true);
  };

  render() {
    console.log('his.state.loggedIn:', this.state.loggedIn)
    if (this.state.loggedIn) {
      return <ChatBox toggleLoggedIn={this.toggleLoggedIn} {...this.props} />;
    }
    return <Auth toggleLoggedIn={this.toggleLoggedIn} {...this.props} />;
  }
}
