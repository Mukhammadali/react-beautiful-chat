import React, { Component } from 'react';

class Header extends Component {
  render() {
    return (
      <div className="sc-header">
        <img className="sc-header--img" src={this.props.imageUrl} alt="" />
        <div className="sc-header--team-name"> {this.props.teamName} </div>
        <div className="sc-header--close-button" onClick={this.props.onClose}>
          <img
            src="https://drbot.sfo2.digitaloceanspaces.com/media/close-icon.png"
            alt=""
          />
        </div>
      </div>
    );
  }
}

export default Header;
