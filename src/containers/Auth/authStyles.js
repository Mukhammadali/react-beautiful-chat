import styled from 'styled-components';

export const AuthWrapper = styled.div`
  /* position: relative; */
  /* width: 400px; */
  padding: 25px;
  /* margin: auto; */
  /* margin-top: 100px; */
  /* border-radius: 10px; */

  h3 {
    margin-top: 0;
    font-size: 24px;
    color: #555;
  }

  .form--item {
    margin-top: 15px;
  }

  label {
    display: block;
    font-size: 16px;
    font-weight: 600px;
    letter-spacing: 0.6px;
    color: #666;
    margin-bottom: 5px;
  }

  input {
    box-sizing: border-box;
    width: 100%;
    margin: 0px;
    padding: 0px;
    background: #fafbfc;
    color: #8da2b5;
    border: 1px solid #dde5ed;
    font-size: 16px;
    padding: 10px 15px 10px;
    margin: 0;
    border-radius: 6px;
    outline: none;
    margin-bottom: 10px;

    &::placeholder {
      color: #999;
    }
  }

  button {
    font-weight: 400;
    margin-top: 20px;
    user-select: none;
    border: none;
    line-height: 1.4;
    text-decoration: none;
    background: #4e8cff;
    color: white;
    padding: 8px 25px;
    font-size: 16px;
    border-radius: 4px;
    box-sizing: border-box;
    outline: none;
    cursor: pointer;
    align-self: center;
  }
`;
