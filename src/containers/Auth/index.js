import React, { Component } from 'react';
import { AuthWrapper } from './authStyles';
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';


const WEBCHAT_AUTH_MUTATION = gql`
  mutation webchatAuth($email: Email!, $name: String!){
    webchatAuth(email: $email, name: $name) {
      _id
      conversationId
      name
      email
    }
  }

`;

class Auth extends Component {
  state = {
    userName: '',
    email: null,
  };

  handleChange = e => {
    e.preventDefault();
    this.setState({ [e.target.name]: e.target.value });
  };

  handleSubmit = async e => {
    e.preventDefault();
    const { userName, email } = this.state;
    if (userName !== '' && email !== null) {
      const self = this;
      this.props.webchatAuth({
        variables: {
          email,
          name: userName
        },
        update: (cache, { data: { webchatAuth } } ) => {
          sessionStorage.setItem('webchat_me', JSON.stringify(webchatAuth))
          self.props.toggleLoggedIn();
        }
      }).catch(err => console.log(err))
    } else {
      alert('Name and Email are required!');
    }
  };

  render() {
    return (
      <AuthWrapper>
        <h3>Sign In</h3>
        <form>
          <div className="form--item">
            <label>Full Name:</label>
            <input
              name="userName"
              type="text"
              placeholder="e.g: John Doe"
              onChange={this.handleChange}
            />
          </div>
          <div className="form--item">
            <label>Email: </label>
            <input
              name="email"
              type="email"
              placeholder="johndoe@gmail.com"
              onChange={this.handleChange}
            />
          </div>
          <button onClick={this.handleSubmit}>Sign In</button>
        </form>
      </AuthWrapper>
    );
  }
}

export default graphql(WEBCHAT_AUTH_MUTATION, { name: 'webchatAuth' })(Auth);
