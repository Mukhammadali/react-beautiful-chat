import React from 'react';
import { ApolloProvider } from 'react-apollo';
import { persistCache } from 'apollo-cache-persist';
import Auth from '../Auth/index';
import ChatLauncher from '../ChatLauncher/index';
import client, { cache } from '../../apollo/apolloClient';

const App = () => {
  persistCache({
    cache,
    storage: sessionStorage,
  });
  return (
    <ApolloProvider client={client}>
      <ChatLauncher />
    </ApolloProvider>
  );
};

export default App;
