import React, { Component } from 'react';

import Launcher from '../../components/Launcher';
import '../../assets/styles';

class ChatLauncher extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newMessagesCount: 0,
      isOpen: false,
    };
  }

  _handleClick = () => {
    this.setState({
      isOpen: !this.state.isOpen,
      newMessagesCount: 0,
    });
  };

  render() {
    return (
      <Launcher
        agentProfile={{
          teamName: 'Webchat',
          imageUrl:
            'https://a.slack-edge.com/66f9/img/avatars-teams/ava_0001-34.png',
        }}
        newMessagesCount={this.state.newMessagesCount}
        handleClick={this._handleClick}
        isOpen={this.state.isOpen}
        onKeyPress={this.onKeyPress}
      />
    );
  }
}

export default ChatLauncher;
