const config = {
  token:
    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1Y2FjMjRhMWJmN2YzMjRhNTA4ZjhhMjYiLCJvcmdhbml6YXRpb25JZCI6IjVjYWMyNDY3ODAxZDAwNGEyZDNmMTk4OSIsInJvbGUiOiJhZG1pbiIsImlhdCI6MTU2MjY5NTI5MCwiZXhwIjoxNTY1Mjg3MjkwfQ.USOxJWV0MLYgI-ow0rsyFMVNgvUEN2pWAeWFaWEWAMM',
  GRAPHQL_ENDPOINT: 'http://localhost:4000/graphql',
  GRAPHQL_SUBSCRIPTION_ENDPOINT: 'ws://localhost:4000/subscriptions',
};

console.log('MODE', process.env.NODE_ENV);

if (process.env.NODE_ENV === 'production') {
  config.GRAPHQL_ENDPOINT = 'https://botility.com/graphql';
  config.GRAPHQL_SUBSCRIPTION_ENDPOINT = 'wss://botility.com/subscriptions';
}

export default config;
