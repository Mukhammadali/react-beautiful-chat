const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = {
  mode: 'production',
  output: {
    path: path.join(__dirname, "dist"),
    filename: "widget.js",
    library: ["chatLib"],
    libraryTarget: "umd",
    publicPath: "dist",
  },
  stats: {
    children: false,
  },
  devServer: {
    historyApiFallback: true,
    port: 3000,
    disableHostCheck: true,
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.css$/i,
        use: ['style-loader', "css-loader"],
      },
      {
        test: /\.(html)$/,
        use: {
          loader: 'html-loader',
          options: {
            attrs: false
          }
        }
      },
      {
        test: /\.(jpe?g|png|gif|svg)$/i,
        loader: "file-loader?name=/assets/[name].[ext]"
      }
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: "index.html"
    }),
     new BundleAnalyzerPlugin()
    // new MiniCssExtractPlugin({
    //   filename: '/assets/css/[name].[contenthash:8].css',
    //   chunkFilename: '/assets/css/[name].[contenthash:8].chunk.css',
    // }),
  ],
  resolve: {
    extensions: ['*', '.js', '.jsx']
  },
}